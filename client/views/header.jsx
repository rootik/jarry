import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';

class Header extends Model{
    constructor() {
        /*
            We get the model and storages for this component
        */
        super({
            models: ['Users']
        });
        this.modelUsers = this.model['Users'];
        this.users = JSON.parse(localStorage.getItem("users"));
    }

    render() {
        return (
            <header>
                <material.Toolbar>
                    <material.ToolbarGroup firstChild={true}>
                        <material.ToolbarTitle text={this.users.email} className="title"/>
                    </material.ToolbarGroup>
                    <material.ToolbarGroup>
                        <material.ToolbarTitle text="Options" />
                        <material.FontIcon className="muidocs-icon-custom-sort" />
                        <material.ToolbarSeparator />
                        <material.IconMenu
                            iconButtonElement={
                            <material.IconButton touch={true}>
                                <NavigationExpandMoreIcon />
                            </material.IconButton>
                            }
                        >
                            <material.MenuItem primaryText="Exit" onClick={this.modelUsers.logout} />
                        </material.IconMenu>
                    </material.ToolbarGroup>
                </material.Toolbar>
            </header>
        );
    }
}

export default Header;