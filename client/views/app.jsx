import React from 'react';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import Header from './header';
import Footer from './footer';
import Main from './main';

// App component - represents the whole app
class App extends Model {
  getChildContext() { return { muiTheme: getMuiTheme(baseTheme) }}
  render() {
    return (
      <div id="wrapper">
        <div className="flex">
          <Header />
          <Main />
          <Footer />
        </div>
      </div>
    );
  }
}

App.childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
};

module.exports = App;