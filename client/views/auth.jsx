import getMuiTheme from 'material-ui/styles/getMuiTheme';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';

class Auth extends Model {
    constructor() {
        /*
            We get the model and storages for this component
        */
        super({ models: ['Users'] });
        this.model = this.model['Users'];
        this.state = {email: '', password:'', checkEmail:''};
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePassword = this.handleChangePassword.bind(this);
        this.sign = this.sign.bind(this);
        this.signup = this.signup.bind(this);
    }

    handleChangeEmail(event) { this.setState({email: event.target.value, checkEmail:event.target.getAttribute("check")}) }
    handleChangePassword(event) { this.setState({password:event.target.value}) }
    getChildContext() { return { muiTheme: getMuiTheme(baseTheme) }}
    
    sign(){
        this.model.signin();
        this.setState({email: '', password:'', checkEmail:''});
    }

    signup(){
        this.model.signup();
        this.setState({email: '', password:'', checkEmail:''});
    }

    render() {
        var check;
        if(this.state.password!="" && this.state.email!="" && this.state.checkEmail=="true") {
            check = false;
        }else check =true;
        return (
            <div className="container">
                <div className="row">
                    <div className="auto">
                        <div className="autorization">
                            <div className="block_input">
                                <material.TextField type="email" 
                                                    pattern="email" 
                                                    id="Email" 
                                                    placeholder="Email" 
                                                    onKeyUp={validation.required} 
                                                    value={this.state.email} 
                                                    onChange={this.handleChangeEmail} />
                                <material.TextField 
                                                    id="Password" 
                                                    type="password" 
                                                    pattern="text" 
                                                    placeholder="Password" 
                                                    onKeyUp={validation.required} 
                                                    value={this.state.password} 
                                                    onChange={this.handleChangePassword} />
                            </div>
                            <div className="block_button">
                                <material.FlatButton disabled={check} required="required" label='Sign In' primary={true} onClick={this.sign}/>
                                <material.FlatButton disabled={check} label='Sign Up' primary={true} onClick={this.signup}/>
                            </div>
                            <div className="message_block" id="message"></div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Auth.childContextTypes = {
    muiTheme: React.PropTypes.object.isRequired,
};

var auth = new Auth();
module.exports = Auth;