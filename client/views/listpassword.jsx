import {Provider,connect} from 'react-redux';

class ListPassword extends Model{
    constructor(){
        super({
            models:['Password']
        });
        this.action = this.model['Password'];
        this.state = {
            password:{}
        };
        this.store_pass = this.action.getStore();
    }

    onfocus(e){ e.target.type = "text" }
    onBlur(e){ e.target.type = "password" }

    list_pass(){
        var store = this.store_pass;
        
        store.subscribe(()=>{
            const input = store.getState().map((item, i)=>{
                return (
                        <div key={i} id={'block' + item.id}>
                            <material.TextField type="password" 
                                                placeholder="new password"
                                                name="password"
                                                value={item.password}
                                                data-id={i}
                                                id={item.id}
                                                onChange={this.action.handleChange}
                                                onFocus={this.onfocus}
                                                onBlur={this.onBlur} />
                            <material.IconButton tooltip="save" className="changePassword" data-id={item.id} onClick={(e)=>this.action.changePass(e)}>
                                <i className="fa fa-pencil"  aria-hidden="true"></i>
                            </material.IconButton>
                            <material.IconButton tooltip="delete" className="changePassword" data-id={item.id} onClick={(e)=>this.action.deletePass(e)}>
                                <i className="fa fa-trash-o" aria-hidden="true"></i>
                            </material.IconButton>
                        </div>
                    );
            })

            this.setState({
                input: input
            });
        });
    }

    render(){
        this.list_pass();

        return(
            <Provider store={this.store_pass}>
                <div className="col-xs-12">
                    <div className="addBlock">
                        <material.TextField id="Password" 
                                            type="password" 
                                            pattern="text" 
                                            placeholder="new password"
                                            onChange={(e) => this.action.users.new_password = e.target.value}
                                            />
                        <material.FlatButton label="add pass" primary={true} onClick={this.action.addPass}/>
                    </div>
                    <div className="list_block">
                        <h3>List Password</h3>
                        {this.state.input}
                    </div>
                </div>
            </Provider>
        );
    }
}

let list = new ListPassword;
export default ListPassword;