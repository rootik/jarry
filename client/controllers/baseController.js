import React from 'react';
import RenderDOM from 'react-dom';
import Router from 'react-router';
import Dispather from 'flux-dispatcher';
import EventEmitter from 'event-emitter';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

export class baseController {
  constructor() {
    require("../lib/bootstrap/_bootstrap.scss");
    require("../views/styles/style.scss");
    require("../lib/font-awesome/scss/font-awesome.scss");
    global.React = React;
    global.RenderDOM = RenderDOM;
    global.Model = require("../classes/model");
    global.Store = require("../classes/store");
    global.render = require("../classes/render").default;
    global.validation = require("../classes/validation").default;
    global.material = require('material-ui');

    render.render();
  }
}

let base = new baseController();
export default base;  