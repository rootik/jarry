class StoreUsers extends Store{
    constructor() {
        super({
            models: ['Users'],
            stores: ['Users']  
        });
    }
}

const usersStore = new StoreUsers;
export default usersStore;