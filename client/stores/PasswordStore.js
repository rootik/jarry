class PasswordStore extends Store {
    constructor(){
        super();
    }

    handleAction(state=[],action){
        switch (action.type) {
            case 'ADD_PASS':
                if(action.item.length == 1 || action.item.length == undefined) {
                    state.push({
                        id:action.item[0].id,
                        password:action.item[0].password
                    });

                    return state;
                }else if(action.item.length>1){
                    for (let item of action.item) {
                        state.push({
                            id:item.id,
                            password:item.password
                        });
                    }
                    return state;
                }

            case 'EDIT_PASS':
                if(action.item.length>=1) {
                    state[action.item[0].id].password = action.item[0].password;
                }
                return state;
                
            default:
                return state
        }
    }
}

const password_store = new PasswordStore;
export default password_store;