class Password extends Model{
    constructor(){
        super({
            stores:['Password']
        });

        this.users = {};
        this.store = this.store['Password'];
        this.localStore = this.createStore(this.store.handleAction);
        this.addPass = this.addPass.bind(this);
        this.handleChange = this.handleChange.bind(this);

        this.http({
            type:"POST",
            url:"/password/getPass",
            data:{id:JSON.parse(localStorage.getItem("users"))._id}
        }).then(result=>{
            var result = JSON.parse(result);
            var items = [];
            var new_items = [];

            for(let item of result) {
                items.push({
                    id:item._id,
                    password:item.password
                });
            }

            this.localStore.dispatch({
                type:"ADD_PASS",
                item:items
            });
        });
    }

    getStore(){ return this.localStore }

    addPass(){
        if(this.users.new_password) {
            this.http({
                type:"POST",
                url:"/password/addPass",
                data:{users:JSON.parse(localStorage.getItem("users")), password: this.users.new_password}
            }).then(result=>{
                let results = JSON.parse(result)[0];
                this.localStore.dispatch({
                    type:"ADD_PASS",
                    item:[{
                        id:results._id,
                        password:results.password
                    }]
                });

                document.getElementById("Password").value = "";
            });
        }else{
            alert("Введите пароль");
        }
    }

    changePass(e){
        var target = e.target;
        
        while(target.getAttribute("data-id")==null || target.tagName=="BODY"){
            target = target.parentNode;
        }
        let id = target.getAttribute("data-id");
        let pass = document.getElementById(id).value;

        if(pass){
            this.http({
                type:"POST",
                url:"/password/editPass",
                data:{users:JSON.parse(localStorage.getItem("users")), password: pass, id:id}
            }).then(result=>{
                let results = JSON.parse(result)[0];
            });
        }else{
            alert("Введите пароль");
        }
    }

    handleChange(e) {
        let id = e.target.getAttribute("data-id");
        this.localStore.dispatch({
            type:"EDIT_PASS",
            item:[{
                id:id,
                password:e.target.value
            }]
        });
    }

    deletePass(e){
        var target = e.target;
        
        while(target.getAttribute("data-id")==null || target.tagName=="BODY"){
            target = target.parentNode;
        }
        let id = target.getAttribute("data-id");

        this.http({
            type:"POST",
            url:"/password/deletePass",
            data:{users:JSON.parse(localStorage.getItem("users")), id:id}
        }).then(result=>{
            let results = JSON.parse(result)[0];

            document.getElementById("block"+id).remove();
        });
    }
}

let passs = new Password();
export default passs;