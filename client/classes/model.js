import React from 'react';

class Model extends React.Component {
    constructor(model) {
        super();
        if(model!=undefined){
            this.store = {};
            this.model = {};

            for (var propModels in model.models) { 
                if (model.models.hasOwnProperty(propModels))
                    this.model[model.models[propModels]] = require("../models/"+model.models[propModels]+"").default;
            }
            for (var propStores in model.stores) {
                if(model.stores.hasOwnProperty(propStores)) {
                    this.store[model.stores[propStores]] = require("../stores/"+model.stores[propStores]+"Store").default;
                }
            }
        }

        this.createStore = require('redux').createStore;
        this.queue = [];
        this.length = 0 ;
    }

    http(params) {
        this.length++;

        return new Promise((resolve,reject)=>{
            if(params.url) {
                var xhr = new XMLHttpRequest();
                
                if(!params.type) params.type = "GET";

                if(params.type == "GET" && params.data) params.data = "";
                xhr.open(params.type, params.url);
                xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

                xhr.onreadystatechange = ()=>{
                    if (xhr.readyState!=4) return;

                    xhr.status!=200 ? reject(xhr.statusText + xhr.status) : resolve(xhr.responseText);
                }
                !params.data ? xhr.send() : xhr.send("data="+JSON.stringify(params.data))
            }
        });
    }
}

module.exports = Model;