class Validation{
    constructor() {
        this.pattern = {
            email: "[A-Za-z 0-9]\@[a-z]",
            text: "[A-Za-z 0-9]"
        }

        this.errorClass = "error";
    }

    required(e) {
        if(e) {
            var target = e.target,
                id = target.id,
                value = target.value,
                pattern = new RegExp(valid.pattern[target.type]);
            var border = target.nextElementSibling.childNodes;
            
            if(value.search(pattern)==-1) {
                target.setAttribute("check", false);
                border[1].classList.add(valid.errorClass);
            }else{
                target.setAttribute("check", true);
                border[1].classList.remove(valid.errorClass);
            }
        }
    }
}

let valid = new Validation();
export default valid;