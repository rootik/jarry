class Render {
    render() {
        let tpl = "";
        localStorage.getItem('template') ? tpl = localStorage.getItem("template") : tpl = "auth";
        document.querySelector("div").setAttribute("id", tpl);
        let App = require('../views/'+tpl+'');
        RenderDOM.render(<App />, document.getElementById(tpl));
    }
}

const render = new Render();
export default render;