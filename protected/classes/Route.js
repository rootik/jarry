module.exports = class Route {
    constructor() {
        let app = express();
        app.use(express.static('views'));
        app.set('views', _dirname + '/views');
        app.engine('html', require('ejs').renderFile);
        app.use(session({secret: 'ssshhhhh'}));

        let BaseController = require(_dir_controllers + "baseController");
        let baseController = new BaseController();
        global.baseController = baseController;
        
        app.get(new RegExp('(?:' + _expansion + ')'), (req, res) => {
            let dir = fs.readFileSync(_dirname + req.url);
            res.setHeader('Content-Type', 'text/js');
            res.end(dir);
        });

        app.use(body_parser.json());
        app.use(body_parser.urlencoded({ extended: true }));

        let modules = fs.readdirSync(_dir_modules);
        let def = [];
        var queue = [];
        for (var prop in modules) {
            app.post(new RegExp('(^\/['+modules+']{0,}\/[A-Z a-z]{0,})'), (req,res) => { console.log("request"); queue.push([req,res]) });
        }

        setInterval(()=>{
            if(queue.length!=0) {
                let Controller, ctrl, get, params, asd = queue.shift();
                var req = asd[0];
                var res = asd[1];
                if (req.url.split("?")) {
                    req.url = req.url.split("?");
                    params = req.url[0].split("/");
                    get = req.url[1];
                } else {
                    params = req.url.split("/");
                }
                
                /*
                    create a model for controller
                */
                global.Model = require(_dir_modules + params[1] + "/" + params[1]);


                /*
                    create a handler for the request from the client
                */
                if (params[1] && params[2]) {
                    Controller = require(_dir_modules + params[1] + "/" + params[1] + "Controller");
                    ctrl = new Controller(req, res, get);
                    ctrl[params[2]]();
                }
            }
        },100);

        app.get("/", (req,res) => { baseController.index(req,res) });

        return app;
    }
}