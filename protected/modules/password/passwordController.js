module.exports = class PasswordController extends Model {
    constructor(req,res,get) {
        super();
        this.req = req;
        this.res = res;
        this.get = get;
    }

    getPass(){
        var body = JSON.parse(this.req.body.data);

        if(body.id) {
            let get = this.getPassword(body.id).then(result=>{
                this.res.end(JSON.stringify(result));
            });
        }
    }

    addPass(){
         var body = JSON.parse(this.req.body.data);

         if(body.users && body.password){
             let add = this.add(body).then(result=>{
                this.res.end(JSON.stringify(result));
             });
         }else{
             this.res.end("Error: not data");
         }
    }

    editPass(){
         var body = JSON.parse(this.req.body.data);

         if(body.users && body.password && body.id){
             let edit = this.edit(body).then(result=>{
                this.res.end(JSON.stringify(result));
             });
         }else{
             this.res.end("Error: not data");
         }
    }

    deletePass(){
        var body = JSON.parse(this.req.body.data);

         if(body.users && body.id){
             let deletes = this.deleted(body).then(result=>{
                this.res.end(JSON.stringify(result));
             });
         }else{
             this.res.end("Error: not data");
         }
    }
}