module.exports = class Password {
    constructor(){
        this.db_connect = "mongodb://"+_hostdb+":"+_portdb+"/"+_dbname+""
    }

    getPassword(id){
        return new Promise((resolve,reject)=>{
            db.connect(this.db_connect,(err, db)=>{
                if (err) reject(err);

                db.collection("list_password", (err,collection)=>{
                    if(err) reject(err);

                    collection.find({id_users:id}).toArray((err,document)=>{
                        resolve(document);
                    });

                    db.close();
                });
            });
        });
    }

    add(data){
        return new Promise((resolve,reject)=>{
            db.connect(this.db_connect,(err, db)=>{
                if (err) reject(err);

                db.collection("list_password", (err,collection)=>{
                    if(err) reject(err);

                    collection.insert({
                        id_users:data.users._id,
                        password:data.password
                    }, (err, id)=>{
                        resolve(id.ops);
                        db.close();
                    });
                });
            });
        });
    }

    edit(data){
        return new Promise((resolve,reject)=>{
            db.connect(this.db_connect,(err, dbs)=>{
                if (err) reject(err);

                dbs.collection("list_password", (err,collection)=>{
                    if(err) reject(err);
                    let o_id = db.ObjectID(data.id);

                    collection.update({
                        _id:o_id
                    },{
                        password:data.password,
                        id_users:data.users._id
                    },{
                        upsert:true
                    });
                });
            });
        });
    }

    deleted(data){
        return new Promise((resolve,reject)=>{
            db.connect(this.db_connect,(err, dbs)=>{
                if (err) reject(err);

                dbs.collection("list_password", (err,collection)=>{
                    if(err) reject(err);
                    let o_id = db.ObjectID(data.id);
                    collection.remove({
                        _id:o_id
                    });

                    resolve("ok");
                });
            });
        });
    }
}