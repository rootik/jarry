module.exports = class Users {
    constructor(){
        this.db_connect = "mongodb://"+_hostdb+":"+_portdb+"/"+_dbname+""
    }

    add(data) {
        return new Promise((resolve,reject)=>{
            var vars = {};

            vars.result = true;

            db.connect(this.db_connect, (err,db)=>{
                if(err) return console.log(err);

                db.collection("users",(err,collection)=>{
                    collection.find({email:data.email}).toArray((err,result)=>{
                        if(result.length == 0) {
                            collection.insert(data);
                            vars.message = "You have registered";
                        }else{
                            vars.result = false;
                            vars.message = 'This email already exists';
                        }

                        resolve(vars);
                        db.close();
                    });
                });
            });
        });
    }

    check(data) {
        return new Promise((resolve,reject)=>{
            var vars = {};

            vars.result = true;

            db.connect(this.db_connect, (err,db)=>{
                if(err) return console.log(err);

                db.collection("users",(err,collection)=>{
                    collection.find({email:data.email}).toArray((err,result)=>{
                        
                        if(result.length == 0) {
                            vars.result = false;
                            vars.message = "worng password or email";
                        }else{
                            if(result[0].password!=data.password){
                                vars.result = false;
                                vars.message = "worng password or email";
                            }else{
                                vars.users = result[0];
                            }
                        }

                        resolve(vars);
                        db.close();
                    });
                });
            });
        });
    }
}