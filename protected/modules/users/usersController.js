module.exports = class UsersController extends Model {
    constructor(req,res,get) {
        super();
        this.req = req;
        this.res = res;
        this.get = get;
    }

    sign_up(){
        var body = JSON.parse(this.req.body.data);

        if(body.email!="" && body.password!="") {
            var add = this.add(body);

            add.then(result=> this.res.end(JSON.stringify(result)));
        }
    }

    sign_in(){
        var body = JSON.parse(this.req.body.data);

        if(body.email!="" && body.password!="") {
            var check = this.check(body);

            check.then(result=> this.res.end(JSON.stringify(result)));
        }
    }
}